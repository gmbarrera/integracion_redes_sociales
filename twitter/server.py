from flask import Flask, request
import base64
import hashlib
import hmac
import json

app = Flask(__name__)
app.config['TESTING'] = True
app.config['DEVELOPMENT'] = True
app.config['DEBUG'] = True

@app.route('/webhook_twitter', methods=['POST', 'GET'])
def webhook_twitter():
    
  if request.method == 'GET':
    msg = request.args.get('crc_token')
    msg = msg.encode('utf-8')

    # creates HMAC SHA-256 hash from incomming token and your consumer secret
    sha256_hash_digest = hmac.new(b'WEAMr4FkC4pl72oLYa7CrhcKa7nu7f6EFEpeF3IFio9kxaHOJl', msg=msg, digestmod=hashlib.sha256).digest()

    # construct response data with base64 encoded hash
    cosa_fea = base64.b64encode(sha256_hash_digest)

    response = {
      'response_token': 'sha256=' + cosa_fea.decode()
    }

    # returns properly formatted json response
    return json.dumps(response)
  
  print(request)
  return 'ok'
  
    
from flask import Flask, request
import base64
import hashlib
import hmac
import json

app = Flask(__name__)
app.config['TESTING'] = True
app.config['DEVELOPMENT'] = True
app.config['DEBUG'] = True

@app.route('/webhook_twitter', methods=['POST', 'GET'])
def webhook_twitter():
    
  # creates HMAC SHA-256 hash from incomming token and your consumer secret
  sha256_hash_digest = hmac.new(b'WEAMr4FkC4pl72oLYa7CrhcKa7nu7f6EFEpeF3IFio9kxaHOJl', msg=request.args.get('crc_token'), digestmod=hashlib.sha256).digest()

  # construct response data with base64 encoded hash
  
  cosa_fea = base64.b64encode(sha256_hash_digest)
  
  response = {
    'response_token': 'sha256=' + cosa_fea.decode()
  }

  # returns properly formatted json response
  return json.dumps(response)
    
    
app.run('0.0.0.0', port=3000)